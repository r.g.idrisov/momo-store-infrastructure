output "external_v4_endpoint" {
  description = "An IPv4 external network address that is assigned to the master."

  value = module.kubernetes.external_v4_endpoint
}

output "internal_v4_endpoint" {
  description = "An IPv4 internal network address that is assigned to the master."

  value = module.kubernetes.internal_v4_endpoint
}

output "cluster_ca_certificate" {
  description = <<-EOF
  PEM-encoded public certificate that is the root of trust for
  the Kubernetes cluster.
  EOF

  value = module.kubernetes.cluster_ca_certificate
}

output "cluster_id" {
  description = "ID of a new Kubernetes cluster."

  value = module.kubernetes.cluster_id
}

output "node_groups" {
  description = "Attributes of yandex_node_group resources created in cluster"

  value = module.kubernetes.node_groups
}

output "service_account_id" {
  description = <<-EOF
  ID of service account used for provisioning Compute Cloud and VPC resources
  for Kubernetes cluster
  EOF

  value = module.kubernetes.service_account_id
}

output "node_service_account_id" {
  description = <<-EOF
  ID of service account to be used by the worker nodes of the Kubernetes cluster
  to access Container Registry or to push node logs and metrics
  EOF

  value = module.kubernetes.node_service_account_id
}
