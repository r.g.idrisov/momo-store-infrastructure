terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = ">= 0.44"
    }
  }
  backend "s3" {
    endpoint = "storage.yandexcloud.net"
    bucket   = "momo-store-idrisov"
    region   = "us-east-1"
    key      = "terraform/tf.tfstate"
    #encrypt = true
    #kms_key_id = ""
    skip_region_validation      = true
    skip_credentials_validation = true
  }

  required_version = ">= 0.13"
}