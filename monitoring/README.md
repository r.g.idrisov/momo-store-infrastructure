# monitoring

Установка prometheus, grafana, alertmanager
1.  helm upgrade --install prometheus prometheus
2.  helm upgrade --install grafana grafana
3.  helm upgrade --install alertmanager alertmanager

* Пароль по умолчанию от Grafana


* admin\admin


* Урлы для внешнего подключения:

* prometheus.task8-2.tk
* grafana.task8-2.tk
* alertmanager.task8-2.tk


Структура чартов

```
├── alertmanager
│   ├── Chart.yaml
│   ├── templates
│   │   ├── configmap.yaml
│   │   ├── deployment.yaml
│   │   ├── _helpers.tpl
│   │   ├── ingress.yaml
│   │   └── services.yaml
│   └── values.yaml
├── grafana
│   ├── Chart.yaml
│   ├── dashboards
│   │   ├── Kubernetes___Compute_Resources___Pod.json
│   │   └── Kubernetes Pods (Prometheus).json
│   ├── templates
│   │   ├── deployment.yaml
│   │   ├── _helpers.tpl
│   │   ├── ingress.yaml
│   │   ├── pvc.yaml
│   │   └── services.yaml
│   └── values.yaml
├── prometheus
│   ├── Chart.yaml
│   ├── prom-app-example.yaml
│   ├── rules
│   │   ├── instances.rules
│   │   └── test.rules
│   ├── templates
│   │   ├── configmap.yaml
│   │   ├── deployment.yaml
│   │   ├── ingress.yaml
│   │   ├── rules.yaml
│   │   └── services.yaml
│   └── values.yaml
└── README.md
```
