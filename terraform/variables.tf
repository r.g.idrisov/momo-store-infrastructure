variable "yc_token" {
  description = "Token for connecting to ya.cloud"
  type        = string
  sensitive   = true
}

variable "cloud_id" {
  description = "cloud id"
  type        = string
  sensitive   = true
}

variable "folder_id" {
  description = "folder_id"
  type        = string
  sensitive   = true
}

variable "zone" {
  description = "zone"
  type        = string
  sensitive   = true
}
