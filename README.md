# Infrastructure repository

## Overview

* YC
* Terraform
* Kubernetes

## Installation guide

### YC
- **Install latest stable yc binary**
   ```bash
   curl https://storage.yandexcloud.net/yandexcloud-yc/install.sh | bash
   ```

- **Creating a profile**
   ```
   To authenticate as a user:

    Get an OAuth token from Yandex.OAuth. To do this, go to the link and click Allow.

    To configure your CLI profile, run the command yc init.

    Enter your OAuth token when prompted by the command.

    Please go to https://oauth.yandex.com/authorize?response_type=token&client_id=
     in order to obtain OAuth token.

    Please enter OAuth token: 

    At the command prompt, select one of the clouds from the list of those you have access to:

    Please select cloud to use:
     [1] cloud1 (id = )
     [2] cloud2 (id = )
    Please enter your numeric choice: 2

    If only one cloud is available, it's selected automatically.

    Select the default folder:

    Please choose a folder to use:
     [1] folder1 (id = )
     [2] folder2 (id = )
     [3] Create a new folder
    Please enter your numeric choice: 1

    Select the default availability zone for Yandex Compute Cloud:

    Do you want to configure a default Yandex Compute Cloud availability zone? [Y/n] Y
    Which zone do you want to use as a profile default?
     [1] ru-central1-a
     [2] ru-central1-b
     [3] ru-central1-c
     [4] Don't set default zone
    Please enter your numeric choice: 2
   ```

    View your CLI profile settings:
    ```bash
    yc config list
    ```

- **Create service account**
   ```bash
   yc iam service-account create --name k8s-manager --folder-id 
   ```

- **Attach editor rights to service account**
   ```bash
   yc iam service-account get k8s-manager
   yc iam service-account list

   yc resource-manager folder add-access-binding default \
     --role editor \
     --subject serviceAccount:
   # This is not required. This service user primarily for prometheus yandex metric
   yc iam access-key create --service-account-name k8s-manager
   ```

### Terraform

Terraform backend S3 bucket must be created manaully, because it's chicken and egg situation

- **Install latest stable terraform binary**
   ```bash
   # Ubuntu/Debian
   curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
   sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
   sudo apt-get update && sudo apt-get install terraform
   # CentOS/Fedora
   sudo yum install -y yum-utils
   sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/RHEL/hashicorp.repo
   sudo yum -y install terraform
   ```

Terraform config vars:
   ```bash
   export TF_VAR_yc_oauth_token=""
   export TF_VAR_yc_cloud_id=""
   export TF_VAR_yc_folder_id=""
   export TF_VAR_yc_access_key=""
   export TF_VAR_yc_secret_key=""
   ```

Terraform backend vars:

   - access_key = ""
   - secret_key = ""

- **Terraform usage**
   ```bash
   cd terraform
   source ~/.yandex-praktikum-devops-diploma-tfvars.txt
   terraform init -reconfigure -backend-config=~/.yandex-praktikum-devops-diploma-backend.txt
   terraform plan
   terraform apply
   terraform destroy
   ```

Zonal cluster
--------------------

Zonal cluster with a nodes placed in the same zone. In addition will be created
network and service account for managing node group.

To provision this configure provider by the environment variables, i.e.

```
export YC_CLOUD_ID=`yc config get cloud-id`
export YC_FOLDER_ID=`yc config get folder-id`
export YC_TOKEN=`yc config get token`
export YC_ZONE=`yc config get compute-default-zone`
```

then run from within this directory

* `terraform init` to get the plugins
* `terraform plan` to see the infrastructure plan
* `terraform apply` to apply the infrastructure build
* `terraform destroy` to destroy the built infrastructure

Terraform Yandex Managed Kubernetes Module
--------------------
Source:

https://registry.terraform.io/modules/sport24ru/managed-kubernetes/yandex/latest

https://github.com/sport24ru/terraform-yandex-managed-kubernetes


### Kubernetes

- **Install latest stable kubectl binary**
   ```bash
   curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
   sudo cp kubectl /usr/local/bin/
   sudo chmod +x /usr/local/bin/kubectl
   ```

- **Install latest stable helm binary**
   ```bash
   curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
   chmod 700 get_helm.sh
   ./get_helm.sh
   ```

- **~/.kube/config generation**
   ```bash
   yc managed-kubernetes cluster get-credentials --id b1g1gq70htss60ug9rif --external
   ```

- **Install autoscaler for Vertical Pod Autoscaler**
   ```bash
   git clone https://github.com/kubernetes/autoscaler.git
   cd autoscaler/vertical-pod-autoscaler/
   ./hack/vpa-up.sh
   ```

- **YAML files for creating general kubernetes object**
   ```bash
   cd kubernetes/additional
   # Creating ClusterIssuer for let's encrypt cert
   kubectl apply -f acme-issuer.yaml
   ```

- **Ingress-nginx install**
   ```bash
   helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
   helm repo update
   helm install ingress-nginx ingress-nginx/ingress-nginx
   ```

- **Cert-manager install**
   ```bash
   helm repo add jetstack https://charts.jetstack.io
   helm repo update
   helm install \
   cert-manager jetstack/cert-manager \
     --namespace cert-manager \
     --create-namespace \
     --version v1.8.2 \
     --set installCRDs=true
   # Validation
   kubectl get Certificates,CertificateRequests,Orders,Challenges --all-namespaces
   ```

- **Deploy Grafana**
   ```bash
   cd k8s/monitoring
   helm upgrade --install grafana ./grafana
   ```

- **Grafana dashboard**

   Use these IDs to import manually dashboards

   - https://grafana.com/grafana/dashboards/1860
   - https://grafana.com/grafana/dashboards/10000
   - https://grafana.com/grafana/dashboards/13639

   Custom dashboards Kubernetes___Compute_Resources___Pod.json and Kubernetes Pods (Prometheus).json is in kubernetes/monitoring/grafana/dashboards directory

- **Alertmanager install**
   ```bash
   cd kubernetes/monitoring
   helm upgrade --install alertmanager ./alertmanager
   ```

- **How to generate API key for prometheus yandex_service_managed-kubernetes job**
   ```bash
   yc iam api-key create --service-account-name k8s-manager \
     --description "this API-key is for k8s-manager (prometheus)"
   ```

- **Prometheus install**
   ```bash
   cd kubernetes/monitoring
   helm upgrade --install prometheus ./prometheus
   ```

- **Prometheus kube-state-metrics install**
   ```bash
   helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
   helm repo update
   helm install kube-state-metrics prometheus-community/kube-state-metrics
   ```

- **Prometheus Node Exporter install**
   ```bash
   #helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
   #helm repo update
   helm install prometheus-node-exporter prometheus-community/prometheus-node-exporter
   ```

- **Deploy Loki and Promtail**
   ```bash
   helm repo add grafana https://grafana.github.io/helm-charts
   helm repo update
   helm upgrade --install loki grafana/loki-stack
   ```
